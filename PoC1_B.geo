// Gmsh project created on Mon Feb 15 10:58:12 2021
SetFactory("OpenCASCADE");
H=10.;
L=10.;
r1=0.78;
r2=2.2;
r3=2.25;

theta= 3.1415926/4; 

h_size=.05;
Nr=((r2-r1))/h_size+1;
//++++++++++++++++++++++++++++++

//+
Point(1) = {0, 0, 0, h_size};
Point(2) = {H, 0, 0, h_size};
Point(3) = {L, H, 0, h_size};
Point(4) = {0, H, 0, h_size};
//+
Point(5) = {r1, 0., 0, h_size};
Point(6) = {r2, 0., 0, h_size};
Point(7) = {r3, 0., 0, h_size};
//+
Point(8) = {0., r1, 0, h_size};
Point(9) = {0., r2, 0, h_size};
Point(10) = {0.,r3, 0, h_size};
//++++++++++++++++++++++++++++++

Line(1) = {10, 4};
Line(2) = {4, 3};
Line(3) = {3, 2};
Line(4) = {2, 7};

Line(5) = {7, 6};
Line(6) = {6, 5};
Line(7) = {8, 9};
//+
Line(8) = {9, 10};
//+


Point(11) = {r1*Cos(theta), r1*Sin(theta), 0, h_size};
Point(12) = {r2*Cos(theta), r2*Sin(theta), 0, h_size};
Point(13) = {r3*Cos(theta), r3*Sin(theta), 0, h_size};

//+
Line(9) = {13, 3};
Line(10) = {13, 12};
Line(11) = {12, 11};


Circle(13) = {10, 1, 13};
Circle(14) = {13, 1, 7};

Circle(15) = {9, 1, 12};
Circle(16) = {12, 1, 6};

Circle(17) = {8, 1, 11};
Circle(18) = {11, 1, 5};
//++++++++++++++++++++++++++++++
Line Loop(1) = {1, 2, -9, -13};
Plane Surface(1) = {1};
Line Loop(2) = {14, -4, -3, -9};
Plane Surface(2) = {2};
Line Loop(3) = {16, -5, -14, 10};
Plane Surface(3) = {3};
Line Loop(4) = {15, -10, -13, -8};
Plane Surface(4) = {4};
Line Loop(5) = {7, 15, 11, -17};
Plane Surface(5) = {5};
Line Loop(6) = {16, 6, -18, -11};
Plane Surface(6) = {6};
//++++++++++++++++++++++++++++++
Transfinite Line {17, 15, 13, 2} = Nr Using Progression 1; //left lines
Transfinite Line {18, 16, 14, 3} = Nr Using Progression 1; //right lines
Transfinite Line {4, 9, 1} = 1.5*Nr Using Progression 1; //left bottom, middle lines
Transfinite Line {8, 10, 5} = (r3-r2)/h_size+1 Using Progression 1; //2nd ring lines
Transfinite Line {7, 11, 6} = Nr Using Progression 1; //1st ring lines

//++++++++++++++++++++++++++++++
Transfinite Surface {1,2,3,4,5,6,7};
//++++++++++++++++++++++++++++++
Recombine Surface {4, 5, 6, 3, 2, 1};

//+
