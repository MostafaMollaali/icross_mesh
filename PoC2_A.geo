// Gmsh project created on Mon Feb 15 10:58:12 2021
SetFactory("OpenCASCADE");
H=10.;
L=10.;

r3=2.25;


h_size=.1;
h_size_coarse =0.5;

//++++++++++++++++++++++++++++++

//+
Point(1) = {0, 0, 0, h_size};
Point(2) = {H, 0, 0, h_size_coarse};
Point(3) = {L, H, 0, h_size_coarse};
Point(4) = {0, H, 0, h_size_coarse};
//+

Point(7) = {r3, 0., 0, h_size};
//+

Point(10) = {0.,r3, 0, h_size};
//+
//+
Line(1) = {10, 4};
Line(2) = {4, 3};
Line(3) = {3, 2};
Line(4) = {2, 7};


Circle(11) = {10, 1, 7};
//+
Line Loop(1) = {1, 2, 3, 4, -11};
Plane Surface(1) = {1};
//+


Recombine Surface {1};
//+
