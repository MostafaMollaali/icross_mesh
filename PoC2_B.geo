// Gmsh project created on Mon Feb 15 10:58:12 2021
SetFactory("OpenCASCADE");
H=10.;
L=10.;
r1=0.78;
r2=2.2;
r3=2.25;

theta= 3.1415926/4; 

h_size=.05;
Nr=((r2-r1))/h_size+1;
//++++++++++++++++++++++++++++++

//+
Point(1) = {0, 0, 0, h_size};
Point(2) = {H, 0, 0, h_size};
Point(3) = {L, H, 0, h_size};
Point(4) = {0, H, 0, h_size};
//+
Point(7) = {r3, 0., 0, h_size};
//+
Point(10) = {0.,r3, 0, h_size};
//++++++++++++++++++++++++++++++

Line(1) = {10, 4};
Line(2) = {4, 3};
Line(3) = {3, 2};
Line(4) = {2, 7};


//+
Point(13) = {r3*Cos(theta), r3*Sin(theta), 0, h_size};

//+
Line(9) = {13, 3};



Circle(13) = {10, 1, 13};
Circle(14) = {13, 1, 7};


//++++++++++++++++++++++++++++++
Line Loop(1) = {1, 2, -9, -13};
Plane Surface(1) = {1};
Line Loop(2) = {14, -4, -3, -9};
Plane Surface(2) = {2};

//++++++++++++++++++++++++++++++
Transfinite Line {13, 2} = Nr Using Progression 1; //left lines
Transfinite Line {14, 3} = Nr Using Progression 1; //right lines
Transfinite Line {4, 9, 1} = 1.5*Nr Using Progression 1; //left bottom, middle lines

//++++++++++++++++++++++++++++++
Transfinite Surface {1,2};
//++++++++++++++++++++++++++++++
Recombine Surface {2, 1};

//+
