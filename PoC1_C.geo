// Gmsh project created on Mon Feb 15 10:58:12 2021
SetFactory("OpenCASCADE");
H=10.;
L=10.;
r1=0.78;
r2=2.2;
r3=2.25;

theta= 3.1415926/4; 

h_size=.05;
Nr=(r3*Cos(theta))/h_size+1;

//++++++++++++++++++++++++++++++
Point(1) = {0, 0, 0, h_size};
Point(2) = {H, 0, 0, h_size};
Point(3) = {L, H, 0, h_size};
Point(4) = {0, H, 0, h_size};
//+
Point(5) = {r1, 0., 0, h_size};
Point(6) = {r2, 0., 0, h_size};
Point(7) = {r3, 0., 0, h_size};
//+
Point(8) = {0., r1, 0, h_size};
Point(9) = {0., r2, 0, h_size};
Point(10) = {0.,r3, 0, h_size};

Point(11) = {r1*Cos(theta), r1*Sin(theta), 0, h_size};
Point(12) = {r2*Cos(theta), r2*Sin(theta), 0, h_size};
Point(13) = {r3*Cos(theta), r3*Sin(theta), 0, h_size};

Point(14) = {r3*Cos(theta), H, 0, h_size};
Point(15) = {L, r3*Sin(theta), 0, h_size};
//++++++++++++++++++++++++++++++

Line(1) = {10, 4};
Line(4) = {2, 7};
Line(5) = {7, 6};
Line(6) = {6, 5};
Line(7) = {8, 9};
Line(8) = {9, 10};

Line(10) = {13, 12};
Line(11) = {12, 11};


Circle(13) = {10, 1, 13};
Circle(14) = {13, 1, 7};

Circle(15) = {9, 1, 12};
Circle(16) = {12, 1, 6};

Circle(17) = {8, 1, 11};
Circle(18) = {11, 1, 5};

Line(19) = {2, 15};
Line(20) = {15, 3};
Line(21) = {3, 14};
Line(22) = {14, 4};
Line(23) = {14, 13};
Line(24) = {13, 15};
//++++++++++++++++++++++++++++++
Curve Loop(1) = {23, 24, 20, 21};
Plane Surface(1) = {1};
//+
Curve Loop(2) = {1, -22, 23, -13};
Plane Surface(2) = {2};
//+
Curve Loop(3) = {14, -4, 19, -24};
Plane Surface(3) = {3};
//+
Curve Loop(4) = {14, 5, -16, -10};
Plane Surface(4) = {4};
//+
Curve Loop(5) = {10, -15, 8, 13};
Plane Surface(5) = {5};
//+
Curve Loop(6) = {7, 15, 11, -17};
Plane Surface(6) = {6};
//+
Curve Loop(7) = {16, 6, -18, -11};
Plane Surface(7) = {7};

//++++++++++++++++++++++++++++++
Transfinite Curve {18, 16, 14, 19} = Nr Using Progression 1; 	//Right arcs
Transfinite Curve {17, 15, 13, 22} = Nr Using Progression 1; 	//Left arcs
Transfinite Curve {4, 24, 21} = 3*Nr Using Progression 1; 	//Horizontal lines
Transfinite Curve {1, 23, 20} = 3*Nr Using Progression 1; 	//Vertical lines
Transfinite Curve {7, 11, 6} = 0.5*Nr+1 Using Progression 1;    //1st ring lines
Transfinite Curve {8, 10, 5} = (r3-r2)/h_size+1 Using Progression 1;    //2nd ring lines
//++++++++++++++++++++++++++++++
Transfinite Surface {1,2,3,4,5,6,7};
//++++++++++++++++++++++++++++++
Recombine Surface {1,2,3,4,5,6,7};//+
//+
