// Gmsh project created on Mon Feb 15 10:58:12 2021
SetFactory("OpenCASCADE");
H=10.;
L=10.;
r1=0.78;
r2=2.2;
r3=2.25;


h_size=.05;
h_size_coarse =0.5;
Nr=((r2-r1))/h_size+1;
//++++++++++++++++++++++++++++++

//+
Point(1) = {0, 0, 0, h_size};
Point(2) = {H, 0, 0, h_size_coarse};
Point(3) = {L, H, 0, h_size_coarse};
Point(4) = {0, H, 0, h_size_coarse};
//+
Point(5) = {r1, 0., 0, h_size};
Point(6) = {r2, 0., 0, h_size};
Point(7) = {r3, 0., 0, h_size};
//+
Point(8) = {0., r1, 0, h_size};
Point(9) = {0., r2, 0, h_size};
Point(10) = {0.,r3, 0, h_size};
//+
//+
Line(1) = {10, 4};
Line(2) = {4, 3};
Line(3) = {3, 2};
Line(4) = {2, 7};

Line(5) = {7, 6};
Line(6) = {6, 5};
Line(7) = {8, 9};
//+
Line(8) = {9, 10};
//+
Circle(9) = {8, 1, 5};
Circle(10) = {9, 1, 6};
Circle(11) = {10, 1, 7};
//+
Line Loop(1) = {1, 2, 3, 4, -11};
Plane Surface(1) = {1};
//+
Line Loop(2) = {8, 11, 5, -10};
Plane Surface(2) = {2};
//+
Line Loop(3) = {7, 10, 6, -9};
Plane Surface(3) = {3};


//+
Transfinite Line {8, 5} = (r3-r2)/h_size+1 Using Progression 1;
Transfinite Line {6, 7} = Nr Using Progression 1;
Transfinite Line {9, 10, 11} = 2*Nr+1 Using Progression 1;//+
//+
Transfinite Surface {3};
Transfinite Surface {2};
//+
Recombine Surface {3};
Recombine Surface {2};
Recombine Surface {1};
//+
